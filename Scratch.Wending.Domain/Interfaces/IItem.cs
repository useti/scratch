﻿using System.Collections.Generic;

namespace Scratch.Wending.Domain.Interfaces
{
    public interface IItem
    {
        int MaxAmount { get; set; }
        int Price { get; set; }
        string Name { get; set; }
        string Description { get; set; }
        ICollection<IItem> AvailableSubitems { get; set; }
        int Id { get; set; }
    }
}