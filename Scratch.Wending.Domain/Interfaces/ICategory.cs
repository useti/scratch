﻿using System.Collections.Generic;

namespace Scratch.Wending.Domain.Interfaces
{
    public interface ICategory
    {
        string Name { get; set; }
        ICollection<IItem> Items { get; set; }
        int Id { get; set; }
    }
}