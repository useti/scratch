﻿using System;
using System.Collections.Generic;

namespace Scratch.Wending.Domain.Interfaces
{
    public interface IOrder
    {
        DateTime DateOrdered { get; set; }
        string Description { get; set; }
        int Total { get; set; }
        ICollection<IOrderItem> OrderItems { get; set; }
        int Id { get; set; }
    }
}