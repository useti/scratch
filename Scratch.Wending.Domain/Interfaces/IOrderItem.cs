﻿using System.Collections.Generic;

namespace Scratch.Wending.Domain.Interfaces
{
    public interface IOrderItem
    {
        IItem SelectedItem { get; set; }
        int Amount { get; set; }
        ICollection<IOrderItem> SubItems { get; set; }
        int Id { get; set; }
    }
}