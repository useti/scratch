﻿using System;
using System.Collections.Generic;
using Scratch.Wending.Domain.Interfaces;

namespace Scratch.Wending.Domain.Classes
{
    public class Order : IOrder
    {
        public DateTime DateOrdered { get; set; }
        public string Description { get; set; }
        public int Total { get; set; }
        public ICollection<IOrderItem> OrderItems { get; set; }
        public int Id { get; set; }

        public Order()
        {
            OrderItems = new HashSet<IOrderItem>();
        }
    }
}