﻿using System.Collections.Generic;
using Scratch.Wending.Domain.Interfaces;

namespace Scratch.Wending.Domain.Classes
{
    public class Item : IItem
    {
        public int MaxAmount { get; set; }
        public int Price { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public ICollection<IItem> AvailableSubitems { get; set; }
        public int Id { get; set; }
    }
}