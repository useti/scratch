﻿using System.Collections.Generic;
using Scratch.Wending.Domain.Interfaces;

namespace Scratch.Wending.Domain.Classes
{
    public class OrderItem : IOrderItem
    {
        public IItem SelectedItem { get; set; }
        public int Amount { get; set; }
        public ICollection<IOrderItem> SubItems { get; set; }
        public int Id { get; set; }
    }
}