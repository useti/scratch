﻿using System.Collections.Generic;
using Scratch.Wending.Domain.Interfaces;

namespace Scratch.Wending.Domain.Classes
{
    public class Category : ICategory
    {
        public string Name { get; set; }
        public ICollection<IItem> Items { get; set; }
        public int Id { get; set; }
    }
}