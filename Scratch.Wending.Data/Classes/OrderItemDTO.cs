﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Scratch.Wending.Domain;

namespace Scratch.Wending.Data.Classes
{
    [Table("ORDERITEM")]
    public class OrderItemDTO
    {
        [Key]
        public int Id { get; set; }

        public int Amount { get; set; } = 0;
        public ICollection<OrderItemDTO> SubItemOf { get; set; }
        public ICollection<OrderItemDTO> SubItems { get; set; }
        public ICollection<OrderDTO> Orders { get; set; }
        public ItemDTO SelectedItem { get; set; }

        public OrderItemDTO()
        {
            SubItems = new HashSet<OrderItemDTO>();
            SubItemOf = new HashSet<OrderItemDTO>();
            Orders = new HashSet<OrderDTO>();
        }
    }
}
