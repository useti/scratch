﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Scratch.Wending.Data.Classes
{
    [Table("ORDERS")]
    public class OrderDTO
    {
        public virtual int Total { get; set; }
        public ICollection<OrderItemDTO> OrderItems { get; set; }
        public string Description { get; set; }

        [Key]
        public int Id { get; set; }

        public DateTime DateOrdered { get; set; }
        public bool IsFixed { get; set; }

        public OrderDTO()
        {
            OrderItems = new HashSet<OrderItemDTO>();
        }
    }
}