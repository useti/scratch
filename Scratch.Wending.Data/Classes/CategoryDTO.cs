﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Scratch.Wending.Domain;

namespace Scratch.Wending.Data.Classes
{
    [Table("CATEGORIES")]
    public class CategoryDTO
    {
        [Key]
        public int Id { get; set; }

        public ICollection<ItemDTO> Items { get; set; }
        public string Name { get; set; }

        public CategoryDTO()
        {
            Items = new HashSet<ItemDTO>();
        }
    }
}