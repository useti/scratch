﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Scratch.Wending.Domain;

namespace Scratch.Wending.Data.Classes
{
    [Table("ITEMS")]
    public class ItemDTO
    {
        public ICollection<ItemDTO> AvailableSubItems { get; set; }
        public ICollection<ItemDTO> MayBeSubItemOf { get; set; }
        public ICollection<CategoryDTO> Categories { get; set; }
        public int Price { get; set; }
        public int MaxAmount { get; set; } = 0;
        public string Name { get; set; }
        public string Description { get; set; }
        [Key]
        public int Id { get; set; }

        public ItemDTO()
        {
            Categories = new HashSet<CategoryDTO>();
            AvailableSubItems = new HashSet<ItemDTO>();
            MayBeSubItemOf = new HashSet<ItemDTO>();
        }
    }
}