﻿using System.Collections.Generic;
using Scratch.Wending.Data.Classes;

namespace Scratch.Wending.Data.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Data.Context>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Data.Context context)
        {
            //  This method will be called after migrating to the latest version.

            var milk = new ItemDTO()
            {
                Description = "Молоко",
                Name = "Молоко",
                Price = 0,
                MaxAmount = 1
            };
            var sugar = new ItemDTO()
            {
                Description = "Сахар",
                Name = "Сахар",
                Price = 0,
                MaxAmount = 5
            };
            var drinkAddidions = new CategoryDTO()
            {
                Name = "Добавки к напиткам",
                Items = new HashSet<ItemDTO>() {sugar, milk}
            };
            var ham = new ItemDTO()
            {
                Description = "Ветчина",
                Name = "Ветчина",
                Price = 0,
                MaxAmount = 1
            };
            var cheese = new ItemDTO()
            {
                Description = "Сыр",
                Name = "Сыр",
                Price = 0,
                MaxAmount = 1
            };
            var foodAdditions = new CategoryDTO()
            {
                Name = "Добавки к еде",
                Items = new HashSet<ItemDTO>() {ham, cheese}
            };
            var latte = new ItemDTO()
            {
                AvailableSubItems = new HashSet<ItemDTO>() {milk, sugar},
                Description = "Эспрессо+молоко",
                Name = "Латте",
                MaxAmount = 1
            };
            var cappuchino = new ItemDTO()
            {
                AvailableSubItems = new HashSet<ItemDTO>() { milk, sugar },
                Description = "Эспрессо+молоко+молочная пенка",
                Name = "Каппучино",
                MaxAmount = 1
            };
            var tea = new ItemDTO()
            {
                AvailableSubItems = new HashSet<ItemDTO>() { sugar },
                Description = "Чай",
                Name = "Чай",
                MaxAmount = 1
            };
            var drinks = new CategoryDTO()
            {
                Name = "Напитки",
                Items = new HashSet<ItemDTO>() {latte, cappuchino, tea}
            };
            var sandvich = new ItemDTO()
            {
                AvailableSubItems = new HashSet<ItemDTO>() {ham, cheese},
                Description = "Хлеб+ветчина",
                Name = "Бутерброд",
                MaxAmount = 1
            };
            var bagle = new ItemDTO()
            {
                AvailableSubItems = new HashSet<ItemDTO>() { ham, cheese },
                Description = "Булочка",
                Name = "Булочка",
                MaxAmount = 1
            };
            var food = new CategoryDTO()
            {
                Name = "Еда",
                Items = new HashSet<ItemDTO>() {sandvich, bagle}
            };

            context.Categories.AddOrUpdate(
                drinkAddidions,
                foodAdditions,
                drinks,
                food
            );
            

        }
    }
}
