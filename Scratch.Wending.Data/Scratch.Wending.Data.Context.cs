﻿using System.Data.Entity;
using Scratch.Wending.Data.Classes;

namespace Scratch.Wending.Data
{
    public class Context: DbContext
    {
		public DbSet<OrderDTO> Orders { get; set; }
		public DbSet<ItemDTO> Items { get; set; }
        public DbSet<OrderItemDTO> OrderItems { get; set; }
        public DbSet<CategoryDTO> Categories { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<OrderDTO>()
                .HasMany(f => f.OrderItems)
                .WithMany(f => f.Orders)
                .Map(m => m.ToTable("L_ORDER_ITEM").MapLeftKey("OrderId").MapRightKey("ItemId"));

            modelBuilder.Entity<CategoryDTO>()
                .HasMany(f => f.Items)
                .WithMany(f => f.Categories)
                .Map(m => m.ToTable("L_CATEGORY_ITEM").MapLeftKey("CategoryId").MapRightKey("ItemId"));

            modelBuilder.Entity<ItemDTO>()
                .HasMany(f => f.AvailableSubItems)
                .WithMany(f => f.MayBeSubItemOf)
                .Map(m => m.ToTable("L_ITEM_SUBITEM").MapLeftKey("ItemId").MapRightKey("SubItemId"));

            modelBuilder.Entity<OrderItemDTO>()
                .HasMany(f => f.SubItems)
                .WithMany(f => f.SubItemOf)
                .Map(m => m.ToTable("L_ORDERITEM_SUBITEM").MapLeftKey("ItemId").MapRightKey("SibItemId"));
        }

        public Context(): base("DefaultConnection")
        { }
    }
}
